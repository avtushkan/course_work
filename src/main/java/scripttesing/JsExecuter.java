package scripttesing;

public interface JsExecuter {
    Object executeScript(Script script) throws ScriptTestException;
    Object executeScript(Script script, String ... args) throws ScriptTestException;
    Object executeScript(String functionName, String[] args) throws ScriptTestException;
}
