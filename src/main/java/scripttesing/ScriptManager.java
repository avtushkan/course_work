package scripttesing;

import javax.script.ScriptEngine;
import java.io.File;

public abstract class ScriptManager {

    private ScriptEngine engine;

    ScriptManager(ScriptEngine engine){
        this.engine = engine;
    }

    ScriptEngine getEngine() {
        return engine;
    }

    public abstract Script downloadScript(File file) throws ScriptTestException;


}
