package scripttesing;

import org.apache.log4j.Logger;

import javax.script.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


public class JsScriptManager extends ScriptManager implements JsExecuter{

    private static final Logger LOGGER = Logger.getLogger(JsScriptManager.class);

    public JsScriptManager(boolean isEnviromentNeeded)  {
        super(new ScriptEngineManager().getEngineByName("nashorn"));
        if(isEnviromentNeeded) {
            try {
                Reader reader =  new FileReader(new File(getClass().getResource("../lib/env.js").getFile()));
                getEngine().eval(reader);
            } catch (IOException | ScriptException e) {
                LOGGER.error(e);
            }
        }
    }

    public void addJSLibrary(File file) throws ScriptTestException {
        try {
            Reader reader = new FileReader(file);
            getEngine().eval(reader);
        } catch (ScriptException | FileNotFoundException e) {
            throw new ScriptTestException(e);
        }
    }

    @Override
    public Script downloadScript(File file) throws ScriptTestException {
        Script script = new Script();
        FileReader reader;
        try {
           reader = new FileReader(file);
        } catch (FileNotFoundException e) {
            throw new ScriptTestException(e);
        }

        script.setReader(reader);
        script.setPath(file.getPath());

        return script;
    }
    public Script downloadScriptFromString(String scriptText){
        Script script = new Script();
        script.setScriptText(scriptText);

        return script;
    }

    @Override
    public Object executeScript(Script script) throws ScriptTestException {
        Object result = null;
        try {
            execute(script);
            if(script.getEvaluationFunctionName()!=null) {
                Invocable invocable = (Invocable) getEngine();
                result = invocable.invokeFunction(script.getEvaluationFunctionName());
            }
        } catch (javax.script.ScriptException | NoSuchMethodException e) {
            throw new ScriptTestException(e);
        }
        return convert(result);
    }

    @Override
    public Object executeScript(Script script, String... args) throws ScriptTestException {
        Object result = null;
        try {
            execute(script);
            if(script.getEvaluationFunctionName()!=null){
                Invocable invocable = (Invocable) getEngine();
                result = invocable.invokeFunction(script.getEvaluationFunctionName(), args);
            }
        } catch (javax.script.ScriptException | NoSuchMethodException e) {
            throw new ScriptTestException(e);
        }
        return convert(result);
    }

    @Override
    public Object executeScript(String functionName, String[] args) throws ScriptTestException {
        Object result;
        Invocable invocable = (Invocable) getEngine();
        try {
            result = invocable.invokeFunction(functionName, args);
        } catch (ScriptException |NoSuchMethodException e) {
            throw new ScriptTestException(e);
        }
        return convert(result);
    }


    private void execute(Script script) throws ScriptTestException {
        try {
            if (script.getContext() != null) {
                if (script.getContext().getReader() != null) {
                    getEngine().eval(script.getContext().getReader());
                }
                if (script.getContext().getScriptText() != null) {
                    getEngine().eval(script.getContext().getScriptText());
                }
            }

            if (script.getReader() != null) {
                getEngine().eval(script.getReader());
            } else {
                getEngine().eval(script.getScriptText());
            }


            if (script.getTestMethod() != null) {
                if (script.getTestMethod().getReader() != null) {
                    getEngine().eval(script.getTestMethod().getReader());
                }
                if (script.getTestMethod().getScriptText() != null) {
                    getEngine().eval(script.getTestMethod().getScriptText());
                }
            }
        }catch (ScriptException e){
            throw new ScriptTestException(e);
        }
    }

    private Object convert(final Object obj) throws ScriptTestException {
        if (obj instanceof Bindings) {
            try {
                final Class<?> cls = Class.forName("jdk.nashorn.api.scripting.ScriptObjectMirror");
                if (cls.isAssignableFrom(obj.getClass())) {
                    final Method isArray = cls.getMethod("isArray");
                    final Object result = isArray.invoke(obj);
                    if (result != null && result.equals(true)) {
                        final Method values = cls.getMethod("values");
                        final Object vals = values.invoke(obj);
                        if (vals instanceof Collection<?>) {
                            final Collection<?> coll = (Collection<?>) vals;
                            Object [] objects = coll.toArray();
                            for(int i = 0; i<objects.length; i++){
                                Object or = convert(objects[i]);
                                objects[i] = or;
                            }
                            return objects;
                        }
                    }
                }
            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException
                    | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new ScriptTestException(e);
            }
        }
        if (obj instanceof List<?>) {
            final List<?> list = (List<?>) obj;
            return list.toArray(new Object[0]);
        }
        return obj;
    }

}
