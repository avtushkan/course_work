package scripttesing;

import java.io.FileReader;

public class Script {

    private FileReader reader;
    private String path;
    private String evaluationFunctionName;
    private Script testMethod;
    private Script context;
    private String scriptText;

    public String getScriptText() {
        return scriptText;
    }

    public void setScriptText(String scriptText) {
        this.scriptText = scriptText;
    }

    public Script getContext() {
        return context;
    }

    public void setContext(Script context) {
        this.context = context;
    }

    public Script getTestMethod() {
        return testMethod;
    }

    public void setTestMethod(Script testMethod) {
        this.testMethod = testMethod;
    }

    public FileReader getReader() {
        return reader;
    }

    public void setReader(FileReader reader) {
        this.reader = reader;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getEvaluationFunctionName() {
        return evaluationFunctionName;
    }

    public void setEvaluationFunctionName(String functionName) {
        this.evaluationFunctionName = functionName;
    }
}
