
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import scripttesing.JsScriptManager;
import scripttesing.Script;
import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;



public class TestDOM {

    private static JsScriptManager scriptManager = null;

    @BeforeClass
    public static void Before() throws Exception{
        scriptManager = new JsScriptManager(true);
        File file = new File(JsScriptManager.class.getResource("/lib/jquery.js").getFile());
        scriptManager.addJSLibrary(file);

    }
    @Before
    public void beforeHelloWorld() throws Exception{
        Script scriptClean = scriptManager.downloadScriptFromString(
                "function clean(){" +
                        " document.body.innerHTML = \"\""  +
                        " }");
        scriptClean.setEvaluationFunctionName("clean");
        scriptManager.executeScript(scriptClean);
    }

    @Test
    public void HelloWorldTest() throws Exception{
        File file = new File(JsScriptManager.class.getResource("/domManipulation/helloWorld.js").getFile());
        Script mainScript = scriptManager.downloadScript(file);
        Script testMethod = scriptManager.downloadScriptFromString(
                "function test(){ " +
                        "    createElement(); " +
                        "    return document; " +
                        "   }"
        );
        mainScript.setTestMethod(testMethod);
        mainScript.setEvaluationFunctionName("test");
        ScriptObjectMirror document = (ScriptObjectMirror) scriptManager.executeScript(mainScript);
        ScriptObjectMirror body = (ScriptObjectMirror)document.get("body");
        ScriptObjectMirror firstChild = (ScriptObjectMirror)body.get("firstChild");
        String text = (String) firstChild.get("innerHTML");
        Assert.assertEquals("Hello World",text);


    }


    @Test
    public void ClickTest() throws Exception{
        File file = new File(JsScriptManager.class.getResource("/domManipulation/click.js").getFile());
        Script mainScript = scriptManager.downloadScript(file);
        Script context = scriptManager.downloadScriptFromString(
                "var p = document.createElement(\"p\"); " +
                "p.setAttribute('id', 'demo'); " +
                "document.body.appendChild(p);");
        mainScript.setContext(context);

        Script testMethod = scriptManager.downloadScriptFromString(
                "function test(){ " +
                        "  $('#demo').trigger('click'); " +
                        "  return $('#demo').is(\":visible\"); " +
                        "}");
        mainScript.setTestMethod(testMethod);
        mainScript.setEvaluationFunctionName("test");
        Boolean result = (Boolean) scriptManager.executeScript(mainScript);
        Assert.assertFalse(result);
    }

    @Test
    public void DateTest() throws Exception{
        File file = new File(JsScriptManager.class.getResource("/domManipulation/date.js").getFile());
        Script mainScript = scriptManager.downloadScript(file);
        Script context = scriptManager.downloadScriptFromString(
                "var p = document.createElement(\"p\"); " +
                        "p.setAttribute('id', 'demo'); " +
                        "document.body.appendChild(p);");
        mainScript.setContext(context);

        Script testMethod = scriptManager.downloadScriptFromString(
                "function test(){ " +
                        "    displayDate(); " +
                        "    return $('#demo').text(); " +
                        "}"
        );
        mainScript.setTestMethod(testMethod);
        mainScript.setEvaluationFunctionName("test");

        LocalDateTime now1= LocalDateTime.now(ZoneId.of("GMT"));

        String result = (String) scriptManager.executeScript(mainScript);
        LocalDateTime resultDate = LocalDateTime.parse(result, DateTimeFormatter.ISO_DATE_TIME);
        LocalDateTime now2= LocalDateTime.now(ZoneId.of("GMT"));
        Assert.assertTrue(resultDate.isAfter(now1) && resultDate.isBefore(now2));
    }



}
