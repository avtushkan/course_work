import org.junit.BeforeClass;
import scripttesing.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;

public class JsTest {
    private static JsScriptManager scriptManager = null;
    @BeforeClass
    public static void Before(){
    scriptManager = new JsScriptManager(false);
    }

    @Test
    public void FactTest() throws Exception{
        File file =new File(getClass().getResource("/simple/factorial.js").getFile());
        Script script = scriptManager.downloadScript(file);
        script.setEvaluationFunctionName("fact");

        double factResult = (double) scriptManager.executeScript(script, "5");
        Assert.assertEquals(120, factResult, 0.01);

    }
    @Test
    public void QuadrEqTest() throws Exception{
        File file =new File(getClass().getResource("/simple/qadratic_equation.js").getFile());
        Script script = scriptManager.downloadScript(file);
        script.setEvaluationFunctionName("square_equation");

        double[] expected = {-0.5,0.7,-0.5,-0.7};
        Object[] squareResult = (Object[]) scriptManager.executeScript(script, "4","4","3");
        for(int i = 0; i < squareResult.length; i++){
            Assert.assertEquals(expected[i], (double) squareResult[i], 0.01);
        }
    }

}
