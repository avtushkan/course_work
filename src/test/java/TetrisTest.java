import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import scripttesing.JsScriptManager;
import scripttesing.Script;

import java.io.File;

/**
 * Created by avtus on 22.04.2018.
 */
public class TetrisTest {
    private static JsScriptManager scriptManager = null;

    @BeforeClass
    public static void Before() throws Exception{
        System.setProperty("nashorn.args", "--language=es6");
        scriptManager = new JsScriptManager(true);
  scriptManager.addJSLibrary(new File(JsScriptManager.class.getResource("/lib/jquery.js").getFile()));
        Script tetrisCanvasElement = scriptManager.
                downloadScriptFromString("var p = document.createElement('canvas'); " +
                        " p.setAttribute('id', 'tetris');  " +
                        "document.body.appendChild(p);" +
                        "var p1 = document.createElement('div');" +
                        "p1.setAttribute('id', 'score');" +
                        "document.body.appendChild(p1);");

        Script tetris =scriptManager.
                downloadScript(new File(JsScriptManager.class.getResource("/Tetris.js").getFile()));

        tetris.setContext(tetrisCanvasElement);;
        scriptManager.executeScript(tetris);


    }


    @Test
    public void createPieceTest() throws Exception{
        String [] t = {"T"};
        Object createdPieceT[] = (Object[]) scriptManager.executeScript(new String("createPiece"),t);
        Object expectedPieceT [][] = {{0,0,0},{1,1,1},{0,1,0}};
        Assert.assertArrayEquals(createdPieceT,expectedPieceT);

        String [] o = {"O"};
        Object createdPieceO[] = (Object[]) scriptManager.executeScript(new String("createPiece"),o);
        Object expectedPieceO[][] = {{2,2},{2,2}};
        Assert.assertArrayEquals(createdPieceO,expectedPieceO);

        String [] l = {"L"};
        Object createdPieceL[] = (Object[]) scriptManager.executeScript(new String("createPiece"),l);
        Object expectedPieceL[][] = {{0,3,0},{0,3,0},{0,3,3}};
        Assert.assertArrayEquals(createdPieceL,expectedPieceL);
    }

    @Test
    public void collideTest() throws Exception{


        Script scriptContext = scriptManager.downloadScriptFromString(
                "  player.matrix = createPiece('T'); " +
                        " player.pos.x = 12; " +
                        " player.pos.y = 1; " );

        Script testScript = scriptManager.downloadScriptFromString("function testCollide(){" +
                " return collide(arena, player); " +
                " }");

        testScript.setContext(scriptContext);
        testScript.setEvaluationFunctionName("testCollide");
        Assert.assertTrue((Boolean) scriptManager.executeScript(testScript));

    }
    @Test
    public void playerMoveTest() throws Exception{
        Script scriptContext = scriptManager.downloadScriptFromString(
                "  player.matrix = createPiece('T'); " +
                        " player.pos.x = 1; " +
                        " player.pos.y = 1; " );

        Script testScript = scriptManager.downloadScriptFromString("function testPlayerMove(){" +
                "playerMove(-1); " +
                " return player.pos.x; " +
                " }");

        testScript.setContext(scriptContext);
        testScript.setEvaluationFunctionName("testPlayerMove");
        Assert.assertEquals(0,(int) scriptManager.executeScript(testScript));


        Script testScript1 = scriptManager.downloadScriptFromString("function testPlayerMove(){" +
                "playerMove(-1); " +
                " return player.pos.x; " +
                " }");

        testScript1.setContext(scriptContext);
        testScript.setEvaluationFunctionName("testPlayerMove");
        Assert.assertEquals(0,(int) scriptManager.executeScript(testScript));
    }

    @Test
    public void eventListenerTest() throws Exception{
        Script scriptContext = scriptManager.downloadScriptFromString(
                "  player.matrix = createPiece('T'); " +
                        " player.pos.x = 5; " +
                        " player.pos.y = 1; " );

        Script testScript = scriptManager.downloadScriptFromString("function eventSimulation(){" +
                " var e = jQuery.Event('keydown'); " +
                " e.which = 37; " +
                " $(document).trigger(e);" +
                " return player.pos.x; "+
                "}");

        testScript.setContext(scriptContext);
        testScript.setEvaluationFunctionName("eventSimulation");
        Assert.assertEquals(4,(int) scriptManager.executeScript(testScript));
    }

}
