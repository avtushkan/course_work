function square_equation(a, b,c) {
    var answer = [];

    var d = b * b - 4 * a * c;
    if ( d < 0 ) {
        answer[0] = - b / ( 2 * a );
        answer[1] =  Math.sqrt( -d ) / ( 2 * a );
        answer[2] = - b / ( 2 * a);
        answer[3] = -Math.sqrt( -d ) / ( 2 * a );
        return answer;

    } else {
        if ( d == 0 ) {

            answer[0] =  -b / ( 2 * a );
            return answer;

        } else {

            answer[0] = -b / ( 2 * a ) - Math.sqrt( d ) / ( 2 * a );
            answer[1] = -b / ( 2 * a ) + Math.sqrt( d ) / ( 2 * a );
            return answer;
        }
    }
};

